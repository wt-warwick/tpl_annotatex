# tpl_annotatex
This repository contains the template for the AnnotateX project.

The project is comprised of 3 repositories: `com_annotatex` that contains the Joomla! extension
implementing the tool, `annotatex-scripts` that contains useful script for the extraction of
the data, and this one for the template.

## Understanding the Code
You can find a nice documentation on how to build a template for the Joomla! platform at this
address:
[https://docs.joomla.org/Creating_a_basic_Joomla!_templatehttps://docs.joomla.org/Creating_a_basic_Joomla!_template](https://docs.joomla.org/Creating_a_basic_Joomla!_template)

You probably won't need to modify the template, but in case you have to the official documentation
is the only way.

## Installation
To install a template, create a zip file of the repository and follow the guide at
[https://docs.joomla.org/J3.x:Installing_a_template](https://docs.joomla.org/J3.x:Installing_a_template).