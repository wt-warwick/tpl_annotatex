<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.tooltip');


// Load user_profile plugin language
$lang = JFactory::getLanguage();
$lang->load('plg_user_profile', JPATH_ADMINISTRATOR);?>

<div class="container profile-edit<?php echo $this->pageclass_sfx; ?>"><?php

	if ($this->params->get('show_page_heading')) {?>
		<div class="row">
			<div class="col-12">
				<div class="page-header">
					<h1><?php echo $this->escape($this->params->get('page_heading'));?></h1>
				</div>
			</div>
		</div><?php
	}?>

	<script type="text/javascript">
		Joomla.twoFactorMethodChange = function twoFactorMethodChange(e) {
			var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

			jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el) {
				if (el.id != selectedPane) {
					jQuery('#' + el.id).hide(0);
				} else {
					jQuery('#' + el.id).show(0);
				}
			});
		}
	</script>

	<form action="<?php echo JRoute::_('index.php?option=com_users&task=profile.save');?>"
	      class="form-validate form-horizontal well"
	      enctype="multipart/form-data"
	      id="member-profile"
	      method="post"><?php

	    // Iterate through the form fieldsets and display each one.
		foreach ($this->form->getFieldsets() as $group => $fieldset) {
			$fields = $this->form->getFieldset($group);

			if (count($fields)) {?>
				<fieldset><?php

					// If the fieldset has a label set, display it as the legend.
					if (isset($fieldset->label)) {?>
						<legend><?php echo JText::_($fieldset->label);?></legend><?php
					}

					if (isset($fieldset->description) && trim($fieldset->description)) {?>
						<p><?php echo $this->escape(JText::_($fieldset->description));?></p><?php
					}?>
					
					<div class="row"><?php

						// Iterate through the fields in the set and display them.
						foreach ($fields as $field) {
							// If the field is hidden, just display the input.
							if ($field->hidden) {
								echo $field->input;
							} else {?>
								<div class="form-group col-md-6"><?php
									echo $field->label;

									if ($field->fieldname === 'password1') {
										// Disables autocomplete ?>
										<input type="password" style="display:none"><?php
									}

									// Add the 'form-control' class to the field
									if ($field->fieldname === 'actionlogsNotify' || $field->fieldname === 'actionlogsExtensions') {
										echo $field->input;
									} else {
										$field->class = 'form-control '.$field->class;
										echo $field->input;
									}?>

								</div><?php
							}
						}?>

					</div>
				</fieldset>

				<hr/><?php
			}
		}
		
		if (count($this->twofactormethods) > 1) {?>
			<fieldset>
				<legend><?php echo JText::_('COM_USERS_PROFILE_TWO_FACTOR_AUTH');?></legend>
				<div class="control-group">
					<div class="control-label">
						<label id="jform_twofactor_method-lbl"
						       for="jform_twofactor_method"
						       class="hasTooltip"
							   title="<?php echo '<strong>' . JText::_('COM_USERS_PROFILE_TWOFACTOR_LABEL') . '</strong><br />' . JText::_('COM_USERS_PROFILE_TWOFACTOR_DESC'); ?>">
							<?php echo JText::_('COM_USERS_PROFILE_TWOFACTOR_LABEL');?>
						</label>
					</div>
					<div class="controls">
						<?php echo JHtml::_('select.genericlist', $this->twofactormethods, 'jform[twofactor][method]', array('onchange' => 'Joomla.twoFactorMethodChange()'), 'value', 'text', $this->otpConfig->method, 'jform_twofactor_method', false);?>
					</div>
				</div>
				<div id="com_users_twofactor_forms_container"><?php
					foreach ($this->twofactorform as $form) {
						$style = $form['method'] == $this->otpConfig->method ? 'display: block' : 'display: none';?>

						<div id="com_users_twofactor_<?php echo $form['method'];?>" style="<?php echo $style;?>">
							<?php echo $form['form']; ?>
						</div><?php
					}?>
				</div>
			</fieldset>
			<fieldset>
				<legend><?php echo JText::_('COM_USERS_PROFILE_OTEPS');?></legend>
				<div class="alert alert-info"><?php echo JText::_('COM_USERS_PROFILE_OTEPS_DESC');?></div><?php
				
				if (empty($this->otpConfig->otep)) {?>
					<div class="alert alert-warning"><?php echo JText::_('COM_USERS_PROFILE_OTEPS_WAIT_DESC');?></div><?php
				} else {
					foreach ($this->otpConfig->otep as $otep) {?>
						<span class="span3">
							<?php echo substr($otep, 0, 4); ?>-<?php echo substr($otep, 4, 4); ?>-<?php echo substr($otep, 8, 4); ?>-<?php echo substr($otep, 12, 4);?>
						</span><?php
					}?>

					<div class="clearfix"></div><?php
				}?>
			</fieldset><?php
		} ?>
		<div class="control-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary validate">
					<?php echo JText::_('JSUBMIT');?>
				</button>
				<a class="btn" href="<?php echo JRoute::_('index.php?option=com_users&view=profile'); ?>" title="<?php echo JText::_('JCANCEL'); ?>">
					<?php echo JText::_('JCANCEL');?>
				</a>
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="profile.save" />
			</div>
		</div>

		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>