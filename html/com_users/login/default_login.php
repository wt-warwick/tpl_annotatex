<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');?>

<div class="container mb-5 mt-5 login<?php echo $this->pageclass_sfx; ?>"><?php
	if ($this->params->get('show_page_heading')) {?>
		<div class="row">
			<div class="col-12">
				<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
			</div>
		</div><?php
	}

	if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') {?>
		<div class="row login-description">
			<div class="col-12"><?php
	}

	if ($this->params->get('logindescription_show') == 1) {
		echo $this->params->get('login_description');
	}

	if ($this->params->get('login_image') != '') {?>
		<img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JText::_('COM_USERS_LOGIN_IMAGE_ALT'); ?>" /><?php
	}

	if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') {?>
			</div>
		</div><?php
	}?>

	<div class="row">
		<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="col-md-4 offset-md-4 form-validate form-horizontal well">
			<fieldset><?php
				echo $this->form->renderFieldset('credentials');

				if ($this->tfa) {
					echo $this->form->renderField('secretkey');
				}

				if (JPluginHelper::isEnabled('system', 'remember')) {?>
					<div class="form-group">
						<div class="form-check">
							<input class="form-check-input" id="remember" name="remember" type="checkbox" value="yes"/>
							<label class="form-check-label" for="remember"><?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME');?></label>
						</div>
					</div><?php
				}?>

				<button type="submit" class="btn btn-block btn-primary"><?php echo JText::_('JLOGIN');?></button>

				<?php $return = $this->form->getValue('return', '', $this->params->get('login_redirect_url', $this->params->get('login_redirect_menuitem')));?>
				<input type="hidden" name="return" value="<?php echo base64_encode($return); ?>" />
				<?php echo JHtml::_('form.token');?>
			</fieldset>
		</form>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-4 offset-md-4">
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset');?>">
				<?php echo JText::_('COM_USERS_LOGIN_RESET'); ?>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 offset-md-4">
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=remind');?>">
				<?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?>
			</a>
		</div>
	</div><?php

	$usersConfig = JComponentHelper::getParams('com_users');

	if ($usersConfig->get('allowUserRegistration')) {?>
		<div class="row">
			<div class="col-md-4 offset-md-4">
				<a class="nav-link" href="<?php echo JRoute::_('index.php?option=com_users&view=registration');?>">
					<?php echo JText::_('COM_USERS_LOGIN_REGISTER');?>
				</a>
			</div>
		</div><?php
	}?>
</div>