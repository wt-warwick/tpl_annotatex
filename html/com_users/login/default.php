<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

if (!empty($this->user->get('cookieLogin')) || $this->user->get('guest')) {
	echo $this->loadTemplate('login');
} else {
	echo $this->loadTemplate('logout');
}