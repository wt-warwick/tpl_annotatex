<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');?>

<div class="container mb-5 mt-5 reset<?php echo $this->pageclass_sfx;?>"><?php
	if ($this->params->get('show_page_heading')) {?>
		<div class="row">
			<div class="col-md-4 offset-md-4 page-header">
				<h1><?php echo $this->escape($this->params->get('page_heading'));?></h1>
			</div>
		</div><?php
	}?>

	<div class="row">
		<form action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request');?>"
			  class="col-md-4 offset-md-4 form-validate form-horizontal well"
			  id="user-registration"
			  method="post"><?php
			foreach ($this->form->getFieldsets() as $fieldset) {?>
				<fieldset><?php
					if (isset($fieldset->label)) {?>
						<p><?php echo JText::_($fieldset->label);?></p><?php
					}

					echo $this->form->renderFieldset($fieldset->name);?>
				</fieldset><?php
			}

			echo JHtml::_('form.token'); ?>

			<button type="submit" class="btn btn-block btn-primary validate"><?php echo JText::_('JSUBMIT');?></button>
		</form>
	</div>
</div>