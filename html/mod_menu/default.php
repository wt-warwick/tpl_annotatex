<?php
/**
 * @package		 Joomla.Site
 * @subpackage	mod_menu
 *
 * @copyright	 Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license		 GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

function renderLink($item) {
    switch ($item->type) {
    case 'separator':
    case 'component':
    case 'heading':
    case 'url':
        require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
        break;
    default:
        require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
        break;
    }
}

$id = '';

if ($tagId = $params->get('tag_id', '')) {
	$id = 'id="'.$tagId.'"';
}?>

<ul class="navbar-nav mod-list" <?php echo $id;?>><?php
	foreach ($list as $i => &$item) {
		$class = ($item->level == 1 ? 'nav-item' : 'dropdown-item').' item'.$item->id;

		if ($item->id == $default_id) {
			$class .= ' default';
		}

		if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id)) {
			$class .= ' current';
		}

		if (in_array($item->id, $path)) {
				$class .= ' active';
		} elseif ($item->type === 'alias') {
			$aliasToId = $item->params->get('aliasoptions');

			if (count($path) > 0 && $aliasToId == $path[count($path) - 1]) {
					$class .= ' active';
			} elseif (in_array($aliasToId, $path)) {
					$class .= ' alias-parent-active';
			}
		}

		if ($item->type === 'separator') {
			$class .= ' divider';
		}

		if ($item->parent) {
			$class .= ' dropdown';
		}

		if ($item->level == 1) {?>
			<li class="<?php echo $class;?>"><?php

            renderLink($item);

			if ($item->deeper) {?>
				<div class="dropdown-menu"><?php
			} else {?>
				</li><?php
			}
		} else {
			renderLink($item);

			if ($item->deeper) {?>
				<div class="dropdown-menu"><?php
			} else if ($item->shallower) {
                for ($i = 0; $i < $item->level_diff; $i++) {
                    echo '</div>';
                }

                if (((int) $item->level) == $item->level_diff + 1) {?>
                    </li><?php
                }
			}
		}
	}?>
</ul>