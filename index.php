<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  tpl_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// The PHP tag closes on the same line of the DOCTYPE to make it appear in the first line of the resulting HTML.
?><!DOCTYPE html>
<html xml:lang="<?php echo $this->language;?>" lang="<?php echo $this->language;?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link crossorigin="anonymous"
	      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	      rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;display=swap"
	      rel="stylesheet"
	      type="text/css">

	<link crossorigin="anonymous"
	      href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	      integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	      rel="stylesheet"
	      type="text/css">

	<link href="<?php echo $this->baseurl.'/templates/'.$this->template;?>/css/template.css"
	      rel="stylesheet"
	      type="text/css">

	<script src="https://code.jquery.com/jquery-3.4.1.min.js"
			integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
			crossorigin="anonymous"></script>
	<script crossorigin="anonymous"
	        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script crossorigin="anonymous"
	        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	        src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"  ></script>
	<script src="<?php echo $this->baseurl.'/templates/'.$this->template;?>/js/template.js"
		    type="text/javascript"></script>

	<jdoc:include type="head"/>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="navbar navbar-expand-lg navbar-light">
					<a class="navbar-brand mr-5">
						<img alt="AnnotateX" height="42" src="<?php echo $this->baseurl.'/templates/'.$this->template.'/img/annotatex.png';?>" width="235" />
					</a>
					<jdoc:include type="modules" name="navigation"/>
				</div>
			</div>
		</div>
	</div>

	<hr/>

	<jdoc:include type="component"/>

	<jdoc:include type="message"/>
</body>
</html>